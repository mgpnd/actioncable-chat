class HomeController < ApplicationController
  def index
    @name =
      if user_signed_in?
        current_user.email
      else
        'Guest'
      end
  end
end
