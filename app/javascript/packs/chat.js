import { createConsumer } from '@rails/actioncable';

const consumer = createConsumer();

const chatSub = consumer.subscriptions.create(
  { channel: "ChatChannel" }, {
    received: (body) => {
      const message = `${body.author}: ${body.message}`;

      const messageEl = document.createElement('div');
      messageEl.className = 'message';
      messageEl.textContent = message;

      const messagesContainerEl = document.getElementById('chat');
      messagesContainerEl.appendChild(messageEl);
    }
  }
)


window.addEventListener('DOMContentLoaded', () => {
  const btnEl = document.getElementById('send-message');
  const textEl = document.getElementById('message-text');

  btnEl.addEventListener('click', () => {
    const messageText  = textEl.value;

    chatSub.send({ message: messageText });
    textEl.value = '';
  });
});
