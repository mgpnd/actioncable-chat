class ChatChannel < ApplicationCable::Channel
  def subscribed
    stream_from "chat"
  end

  def receive(body)
    puts "Received: #{body.inspect}"
    ActionCable.server.broadcast(
      'chat',
      {
        author: current_user.email,
        message: body['message']
      }
  )
  end
end
