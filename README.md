# ActionCable Chat

## Installation

Install Ruby dependencies
```sh
bundle install
```

Install JavaScript dependencies (make sure you have [Yarn](https://yarnpkg.com/) installed)
```sh
yarn install
```

Create development database
```sh
bin/rails db:create
```

Apply schema changes to development database
```sh
bin/rails db:migrate
```

Run the server
```sh
bin/rails s
```

**Yay! You're on Rails!**
